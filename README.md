# Practica1

NOMBRE: Practica de fuente rectificada.

INTRODUCCION: Construiremos una fuente rectificada con  un puente de diodos y un regulador de voltaje LM317 y con esto podremos medir los voltajes rectificados, de rizo y los voltajes regulados al igual  que veremos la forma de la onda en cada paso de la fuente. 

MATERIAL UTILIZADOS: 
4 diodos 1N4001
1 resistencia de 10k ohms
1 resistencia de 220 ohms
1 regulador de voltaje LM317
2 capacitores de 1 micro faradio
1 transformador de 12v a 3AM
Alambre de cobre calibre 22
